# otus_task14_Backups

Настроить стенд Vagrant с двумя виртуальными машинами server и client.

Настроить политику бэкапа директории /etc с клиента:
1) Полный бэкап - раз в день
2) Инкрементальный - каждые 10 минут
3) Дифференциальный - каждые 30 минут

Запустить систему на два часа. Для сдачи ДЗ приложить list jobs, list files jobid=<id>
и сами конфиги bacula-*

* Настроить доп. Опции - сжатие, шифрование, дедупликаци
---------------------------------------------------------------------------------------------------

Я буду использовать самые практичные и простые инструменты, для быстрой настройки - BorgBackup с шифрованием репозитория и сжатием, 
который представляет из себя инкрементный бэкапс дедупликацией + borgmatic - простая обвязка над ним для автоматического бэкапа, 
верификации и очистки места. Бэкапируется только дэльта, данные летят по ssh, все это запускается по крону.
 

У нас будет 2 хоста : backup (192.168.100.10) - бэкап сервер, client1 (192.168.100.11) - сервер с которого будем делать бэкап

На backup сервер сделаем специального пользователя borg для бэкапов

Установил BorgBackup на оба

Генерим ключ на клиенте  `[root@client1 ~]# ssh-keygen -b 2048 -t rsa -q -N '' -f ~/.ssh/id_rsa` и публичный ключ прописываем в /home/borg/.ssh/authorized_keys на сервере. 
Можно на клиенте сделать конфиг cat /root/.ssh/config
 ```
Host backup
User borg
HostName 192.168.100.10
Port 22
 
 ```
 Делаем репозиторий на backup сервере, на клиент поставим  borgmatic
 ```
 [root@client1 ~]# borg init --encryption=repokey borg@backup:/home/borg/backups
 pip3 install --user --upgrade borgmatic
 export PATH=$PATH:/root/.local/bin
 generate-borgmatic-config
 
 ```
 Настраиваем конфиг borgmatic
 ```
 [root@client1 ~]# grep -ve '^[[:space:]]*\(#\|$\)' /etc/borgmatic/config.yaml
location:
    source_directories:
        - /etc
    repositories:
        - borg@backup:/home/borg/backups
storage:
    encryption_passphrase: "otus"
    compression: lz4
retention:
    keep_minutely: 6
    keep_hourly: 24
    keep_daily: 7
consistency:
    checks:
        - repository
        - archives
    check_last: 6
 ```
 Проверяем конфиг
 ```
 [root@client1 ~]# validate-borgmatic-config
All given configuration files are valid: /etc/borgmatic/config.yaml
 ```
 Запускаем бэкап
 ```
 [root@client1 ~]# borgmatic --verbosity 1
 ...
 A /etc/postfix/virtual
------------------------------------------------------------------------------
Archive name: client1-2019-10-08T13:43:48.156782
Archive fingerprint: b1a16866e4f00a015c67f629dd578a96fb77a7b50ff672d39e0f6294bcdfe1c7
Time (start): Tue, 2019-10-08 13:43:49
Time (end):   Tue, 2019-10-08 13:43:54
Duration: 5.16 seconds
Number of files: 1702
Utilization of max. archive size: 0%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:               27.83 MB             13.28 MB             11.64 MB
All archives:               27.83 MB             13.28 MB             11.64 MB
                       Unique chunks         Total chunks
Chunk index:                    1288                 1704
------------------------------------------------------------------------------
borg@backup:/home/borg/backups: Running consistency checks
Remote: Starting repository check
Remote: Starting repository index check
Remote: Completed repository check, no problems found.
Starting archive consistency check...
--last 6 archives: only found 1 archives
Analyzing archive client1-2019-10-08T13:43:48.156782 (1/1)
Orphaned objects check skipped (needs all archives checked).
Archive consistency check complete, no problems found.

summary:
/etc/borgmatic/config.yaml: Successfully ran configuration file
 
 ```
 Настроим крон
 ```
 cat borgmatic
 */10 * * * * root PATH=$PATH:/usr/bin:/usr/local/bin /root/.local/bin/borgmatic --syslog-verbosity 1
 systemctl restart crond
 ```
 Профит, последние 2 бэкапа сделал крон
 ```
 [borg@backup ~]$ borg list backups/
Enter passphrase for key /home/borg/backups:
client1-2019-10-08T13:43:48.156782   Tue, 2019-10-08 13:43:49 [b1a16866e4f00a015c67f629dd578a96fb77a7b50ff672d39e0f6294bcdfe1c7]
client1-2019-10-08T14:08:42.471008   Tue, 2019-10-08 14:08:43 [635bbb076cf029842d64a9423ae947fe5d10540cf4eceb472085caa99d6b68ef]
client1-2019-10-08T14:09:40.720064   Tue, 2019-10-08 14:09:41 [ad3078093abdea5537a2991865f0a5fb8838765146edef75128b18c63eeb7005]
client1-2019-10-08T14:10:18.178052   Tue, 2019-10-08 14:10:19 [d752332c7adcaac99c097b824059204f6117a4a681e80943253d9613b3949d6c]
client1-2019-10-08T14:14:26.122545   Tue, 2019-10-08 14:14:27 [00dc2089e59ebbc1acdf336aa162a419539d9f8b4b1d77e9c02c1f4ff205284a]
client1-2019-10-08T14:20:03.194755   Tue, 2019-10-08 14:20:04 [a89cd4746d92ef50910468aa1df62fa7a7fde4f451e78e0364023f1f9691d3ef]
client1-2019-10-08T14:30:03.350928   Tue, 2019-10-08 14:30:04 [b940ac8a610db2b4a8de889e8a45c6ae2643593770241fbde858d6c0264e3b60]
 ```
 
 